# WPS connector

Master: [![Build status](https://ci.appveyor.com/api/projects/status/wklclqbe8f9xvoir/branch/master?svg=true)](https://ci.appveyor.com/project/VladislavAntonyuk/wps-connector/branch/master)

Project: [![Build status](https://ci.appveyor.com/api/projects/status/wklclqbe8f9xvoir?svg=true)](https://ci.appveyor.com/project/VladislavAntonyuk/wps-connector)

Connect to wifi point using wps.

## Features
- List of points
- List of known pins
- Brute force

**In future**

- Cross-platform application