
using System.Collections.Generic;

namespace wifi
{
    public class Network
    {
        public string Name{get;set;}
        public uint Signal{get;set;}
        public string Mac{get;set;}
        public List<string> Pins{get;set;} = new List<string>();

        public override string ToString() {
            return $"{Name}\n{Signal}%\t{Mac}\t{Pins.Count}\n";
        }
    }
}